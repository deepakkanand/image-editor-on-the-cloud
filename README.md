# Image Editor on the Cloud #

This is a basic Image Editor created (while exploring the capabilities of the Platform) using HTML5 Canvas.

Demo: https://drive.google.com/file/d/18MRyU_58RGnf2nI6M103vjdPiWEpEKyF/view?usp=sharing

### Playground ###

In case if you want to try it: https://imageforce-developer-edition.ap6.force.com/

### How is it built ###

This is built using Visualforce due to the Locker Service Limitations (in Aura) imposed on how you can manipulate 
the DOM and HTML5 Canvas.

### How do I get set up? ###

Download the source and deploy it using any Metadata Tools like Workbench or Ant.

### Who do I talk to? ###

Feel free to use it as you wish. In case if you have any queries, please reach out to - Deepak Anand (deepak@dazeworks.com)

### Disclaimer ###

The App is NOT tested. So do expect bugs. 😂