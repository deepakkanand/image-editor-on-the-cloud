public class SFDCImageEditorController {
    public static final String SaveToRecordFeed = 'file-save-to-record';
    public static final String SaveToYourFeed = 'file-save-to-feed';
    public static final String SaveAsAttachment = 'file-save-as-attachment';

    public static final String InvalidRecord = 'INVALID_RECORD_ID';
    public static final String SampleTestMessage = 'POST TO CHECK FOR SUPPORT';
    public static final String ChatterUnSupported = 'CHATTER_NOT_SUPPORTED';

    public class Response {
        public Boolean IsSuccess;
        public String Message;

        public Response(Boolean isSuccess, String msg) {
            this.IsSuccess = isSuccess;
            this.Message = msg;
        }
    }

    public void saveImage() {
        String recordId, fileName, body, imageData, operation;

        recordId = ApexPages.currentPage().getParameters().get('rid');
        fileName = ApexPages.currentPage().getParameters().get('fn');
        body = ApexPages.currentPage().getParameters().get('txt');
        imageData = ApexPages.currentPage().getParameters().get('dat');
        operation = ApexPages.currentPage().getParameters().get('op');

        if (
            operation == SaveToRecordFeed ||
            operation == SaveToYourFeed
        ) {
            SObject feedItem = (SObject) Type.forName('FeedItem').newInstance();

            if(operation == SaveToYourFeed)
            	feedItem.put('ParentId', UserInfo.getUserId());
            else
                feedItem.put('ParentId', recordId);
            
            feedItem.put('Body', body);
            feedItem.put('ContentFileName', fileName);
            feedItem.put('ContentData', EncodingUtil.base64Decode(imageData.subString(22)));

            try {
                INSERT feedItem;
            } catch (DMLException dmlEx) {}
        } else if (operation == SaveAsAttachment) {
            Attachment att = new Attachment(
                ParentId = recordId,
                Name = fileName,
                Body = EncodingUtil.base64Decode(imageData.subString(22)),
                Description = body
            );

            try {
                INSERT att;
            } catch (DMLException dmlEx) {}
        }
    }

    @RemoteAction
    public static Boolean isChatterEnabled() {
        return ConnectApi.Organization.getSettings().features.chatter;
    }

    @RemoteAction
    public static String isRecordValid(String rid, String op) {
        Id recordId = rid;
        List < SObject > record = new List < SObject > ();
        String soql = 'SELECT Id FROM {0} WHERE Id = \'{1}\'';

        soql = soql.replace('{0}', String.valueOf(recordId.getSObjectType())).replace('{1}', rid);

        try {
            record = Database.query(soql);
        } catch (Exception ex) {
            return JSON.serialize(new Response(FALSE, InvalidRecord));
        }

        if (
            op == SaveToRecordFeed ||
            op == SaveToYourFeed
        ) {
            try {
                SObject feedItem = (SObject) Type.forName('FeedItem').newInstance();

                feedItem.put('ParentId', rid);
                feedItem.put('Body', SampleTestMessage);

                INSERT feedItem;

                DELETE feedItem;

                return JSON.serialize(new Response(TRUE, NULL));
            } catch (Exception ex) {
                return JSON.serialize(new Response(FALSE, ChatterUnSupported));
            }
        } else {
            return JSON.serialize(new Response(TRUE, NULL));
        }
    }
}